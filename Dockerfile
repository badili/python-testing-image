FROM osgeo/gdal:ubuntu-full-3.5.3

MAINTAINER Wangoru Kihara wangoru.kihara@badili.co.ke

RUN apt-get update && apt-get install -y python3-pip git wget mariadb-server libmysqlclient-dev mariadb-client
# add pillow dependancies
RUN apt-get install -y python3-dev python3-setuptools libtiff-dev libjpeg8-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev zlib1g-dev libffi-dev libopenjp2-7-dev

RUN pip install pandas==1.5.0
RUN pip install matplotlib==3.3.4
RUN pip install cryptography==3.3.2
RUN pip install plotly==5.13.0
RUN pip install numpy==1.24.2

# start the db server and ran the migrations
RUN service mysql start
